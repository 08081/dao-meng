package com.xiaodao.demo;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;

/**
 * Created by xiaodao
 * date: 2019/8/1
 */
public class MyPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {
        PasswordEncoder passwordEncoder =new SCryptPasswordEncoder();
        return passwordEncoder.encode(charSequence.toString());
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        PasswordEncoder passwordEncoder =new SCryptPasswordEncoder();
        return passwordEncoder.
        matches(charSequence,s);
    }
}
