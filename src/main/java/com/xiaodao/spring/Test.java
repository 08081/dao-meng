package com.xiaodao.spring;

import com.xiaodao.spring.config.AppConfig;
import com.xiaodao.spring.service.CardService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        applicationContext.getBean(CardService.class).list("1");
    }
}
