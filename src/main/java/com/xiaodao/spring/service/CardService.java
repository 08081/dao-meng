package com.xiaodao.spring.service;

import com.xiaodao.spring.dao.CardDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardService {
	@Autowired
	public CardDao cardDao;

	public void list(String number){
		System.out.println(cardDao.list(number));
	}
}
