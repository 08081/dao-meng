package com.xiaodao.spring.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * zqp 接口如何变成一个对象的呢? mapscan把接口变成一个对象 并且注册到spring容器中
 */
public interface CardDao {

	@Select("select * from card where card_number like '%${number}%'")
	public List<Map<Integer,String>> list(@Param("number") String number);
}
