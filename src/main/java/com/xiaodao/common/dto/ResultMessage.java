package com.xiaodao.common.dto;




public class ResultMessage {

	private Header header;

	private Object data;

	public ResultMessage() {
		super();
	}

	public ResultMessage(Header header, Object data) {
		super();
		this.header = header;
		this.data = data;
	}

	public ResultMessage(Header header) {
		super();
		this.header = header;
	}

	public ResultMessage(int code, String message) {
		super();
		this.header = new Header(code, message);
	}

	public ResultMessage(int code, String message, Object data) {
		super();
		this.header = new Header(code, message);
		this.data = data;
	}

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * 返回成功 不带数据
	 * 
	 * @param msg
	 * @return
	 */
	public static ResultMessage successMsg(String msg) {
		return new ResultMessage(0, msg);
	}
	public static ResultMessage successMsg() {
		return new ResultMessage(0, "SUCCESS");
	}

	/**
	 * 返回成功 带数据
	 * 
	 * @param msg
	 * @param data
	 * @return
	 */
	public static ResultMessage successMsg(String msg, Object data) {
		return new ResultMessage(0, msg, data);
	}
    public static ResultMessage successMsg( Object data) {
        return new ResultMessage(0, "SUCCESS", data);
    }
	/**
	 * 返回分页数据
	 * 
	 * @param page
	 * @return
	 */
//	public static ResultMessage successPage(Page<?> page) {
//		Map<String, Object> map = new HashMap<>();
//		SimplePage sp = new SimplePage(page);
//		map.put("page", sp);
//		map.put("list", (List<?>) page);
//		return new ResultMessage(0, "ok", map);
//	}

	/**
	 * 返回错误
	 * 
	 * @param msg
	 * @return
	 */
	public static ResultMessage errorMsg(String msg) {
		return new ResultMessage(-1, msg);
	}

	/**
	 * 返回带code错误
	 * 
	 * @param msg
	 * @param code
	 * @return
	 */
	public static ResultMessage errorMsg(String msg, int code) {
		return new ResultMessage(code, msg);
	}
	public static ResultMessage errorMsg(String msg, Object data) {
		return new ResultMessage(-1, msg,data);
	}

}

