package com.xiaodao.common.config;

import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.injector.LogicSqlInjector;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisMapperRefresh;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;

@Configuration
//@MapperScan("com.cloud.*.dao.*")
@AutoConfigureAfter(SqlSessionFactory.class)
public class MybatisPlusConfig {
 
//	@Autowired
//	private SqlSessionFactory sqlSessionFactory;
 
 
	@Value("${mybatis-plus.mapper-locations}")
	private String mapperLocations;
 
	@Value("${mybatis-plus.refresh-mapper}")
	private Boolean refreshMapper;
 
	private static final ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
	/*
	    * 分页插件，自动识别数据库类型
	    * 多租户，请参考官网【插件扩展】
	    */
	   @Bean
	   public PaginationInterceptor paginationInterceptor() {
	      return new PaginationInterceptor();
	   }
 
 
	   /* * oracle数据库配置JdbcTypeForNull
	    * 参考：https://gitee.com/baomidou/mybatisplus-boot-starter/issues/IHS8X
	    不需要这样配置了，参考 yml:
	    mybatis-plus:
	      confuguration
	        dbc-type-for-null: 'null' */
	   @Bean
	   public ConfigurationCustomizer configurationCustomizer(){
	       return new MybatisPlusCustomizers();
	   }
 
	   class MybatisPlusCustomizers implements ConfigurationCustomizer {
 
	       @Override
	       public void customize(org.apache.ibatis.session.Configuration configuration) {
	           configuration.setJdbcTypeForNull(JdbcType.NULL);
	       }
	   }
 
	/**
	 * mapper.xml 热加载
	 * @return
	 */
	@Bean
	public MybatisMapperRefresh mybatisMapperRefresh(){

		SqlSessionFactory sqlSessionFactory = (SqlSessionFactory) SpringContextHolder.getBean("sqlSessionFactory");
		Resource[] resources = new Resource[0];
		try {
			resources = resourceResolver.getResources(mapperLocations);
		} catch (IOException e) {
			e.printStackTrace();
		}
		MybatisMapperRefresh mybatisMapperRefresh = new MybatisMapperRefresh(resources,sqlSessionFactory,10,5,refreshMapper);
		return mybatisMapperRefresh;
 
	}
 
	/**
	 * 逻辑删除
	 * @return
	 */
	@Bean
	public ISqlInjector sqlInjector() {
		return new LogicSqlInjector();
	}
 
 
 
 
//	    @Bean
//	    @ConfigurationProperties("spring.datasource.druid" )
//	    public DataSource dataSource() {
//	        return DruidDataSourceBuilder
//	                .create()
//	                .build();
//	    }
 
 
 
}