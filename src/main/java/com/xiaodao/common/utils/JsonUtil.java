package com.xiaodao.common.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonUtil {
	
	private final static Log logger = LogFactory.getLog(JsonUtil.class);
	
    private static ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, true);
//        objectMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES,true);
        objectMapper.configure(SerializationFeature.FLUSH_AFTER_WRITE_VALUE,true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
    }

   /**
    *obj 转 json 
   * @param obj
   * @return
    */
    public  static String toJson(final Object obj) {
        String json;
		try {
			json = objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new  RuntimeException(e.getMessage());
		}
      
        return json;
    }

   /**
    * json 转 obj
   * @param json
   * @param clazz
   * @return
    */
    public static  <T> T toBean(final String json, final Class<T> clazz) {
    	if(json==null || clazz==null) return null;
        T t;
		try {
			t = objectMapper.readValue(json, clazz);
		} catch (IOException e) {
			e.printStackTrace();
			throw new  RuntimeException(e.getMessage());
		}
       
        return t;
    }
    
    /**
     * byte数组转 obj
    * @param byteArr
    * @param clazz
    * @return
     */
    public static  <T> T toBean(final byte[] byteArr, final Class<T> clazz) {
        return toBean(new String(byteArr),clazz);
    }
    
    /**
     * json转 list
    * @param json
    * @param ref	例如 new TypeReference<List<TestJson>>() {}
    * @return	
    * @Author: wangxingfei
    * @Date: 2016年11月17日
     */
    public static  <T> List<T> toList(final String json, TypeReference<List<T>> ref) {
    	List<T> t;
		try {
			t = objectMapper.readValue(json, ref);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new  RuntimeException(e.getMessage());
		}
    	
    	return t;
    }
   /**
    * json转 list
   * @param byteArr
   * @param ref
   * @return
   * @Author: liduo
   * @Date: 2017年1月4日
    */
    public static  <T> List<T> toList(final byte[] byteArr, TypeReference<List<T>> ref) {
    	List<T> t;
		try {
			t = objectMapper.readValue(new String(byteArr), ref);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new  RuntimeException(e.getMessage());
		}
    	
    	return t;
    }

	/**
	 * 判断是否是正确的JSON格式
	 *  Jackson library
	 * @param jsonInString
	 * @return
	 */
	public final static boolean isJSON(String jsonInString ) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			mapper.readTree(jsonInString);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
    
    public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException{

    	Map<String,String> map = new HashMap<>();
    	map.put("na","woola");
    	map.put("na11","woola1");
		System.out.println(toJson(map));
    }
    
    
	
}

