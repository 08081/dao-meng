package com.xiaodao.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaodao.modules.system.entity.Dept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by xiaodao
 * date: 2019/8/6
 */
public interface DeptMapper extends BaseMapper<Dept> {


    void batchUpdateLevel(@Param("depts") List<Dept> depts);
    int countByNameAndParentId(@Param("name") String name, @Param("parentId") String parentId,@Param("id") String id );
}
