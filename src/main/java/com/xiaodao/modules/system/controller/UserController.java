package com.xiaodao.modules.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaodao.common.dto.ResultMessage;
import com.xiaodao.modules.system.entity.User;
import com.xiaodao.modules.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by xiaodao
 * date: 2019/8/5
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;
    @GetMapping("/list")
    public List<User> list(){
        return userService.selectUserPage(new Page(0,100),new User());
    }

    @PostMapping("/save")
    public ResultMessage save(@RequestBody  User user){
        userService.save(user);
        return ResultMessage.successMsg();
    }
}
