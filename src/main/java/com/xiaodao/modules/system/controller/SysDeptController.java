package com.xiaodao.modules.system.controller;

import com.xiaodao.common.dto.ResultMessage;
import com.xiaodao.modules.system.dto.DeptDto;
import com.xiaodao.modules.system.entity.Dept;
import com.xiaodao.modules.system.service.IDeptService;
import com.xiaodao.modules.system.service.impl.DeptServiceImpl;
import com.xiaodao.modules.system.service.impl.SysTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by xiaodao
 * date: 2019/8/6
 */
@RestController
@RequestMapping("/sys/dept/")
public class SysDeptController {

    @Autowired
    private DeptServiceImpl deptService;
    @Autowired
    private SysTreeService sysTreeService;

    @PostMapping("save")
    public ResultMessage save(@RequestBody Dept dept) {
        deptService.insert(dept);
        return ResultMessage.successMsg();
    }

    @PostMapping("update")
    public ResultMessage update(@RequestBody Dept dept) {
        deptService.update(dept);
        return ResultMessage.successMsg();
    }

    @GetMapping("tree")
    public ResultMessage deptTree() {
        List<DeptDto> deptDtos = sysTreeService.deptTree();
        return ResultMessage.successMsg(deptDtos);
    }
}
