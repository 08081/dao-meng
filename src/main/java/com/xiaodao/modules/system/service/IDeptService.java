package com.xiaodao.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaodao.modules.system.entity.Dept;

import java.util.List;

/**
 * Created by xiaodao
 * date: 2019/8/6
 */
public interface IDeptService extends IService<Dept> {

//    void batchUpdateLevel(List<Dept> depts);
}
