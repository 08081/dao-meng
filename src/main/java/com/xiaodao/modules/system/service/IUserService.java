package com.xiaodao.modules.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaodao.modules.system.entity.User;

import java.util.List;

/**
 * Created by xiaodao
 * date: 2019/8/5
 */
public interface IUserService extends IService<User> {
    List<User> selectUserPage(IPage page, User user);
}
