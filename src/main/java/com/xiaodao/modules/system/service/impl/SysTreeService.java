package com.xiaodao.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.xiaodao.modules.system.dto.DeptDto;
import com.xiaodao.modules.system.entity.Dept;
import com.xiaodao.modules.system.service.IDeptService;
import com.xiaodao.modules.system.utils.LevelUtil;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by xiaodao
 * date: 2019/8/6
 */
@Service
public class SysTreeService {


    @Autowired
    private IDeptService deptService;



    public List<DeptDto> deptTree() {
        QueryWrapper<Dept> queryWrapper = new QueryWrapper<>();
        List<DeptDto> dtoList = deptService.list(queryWrapper).stream().map(o -> DeptDto.transform(o)).collect(Collectors.toList());
        return deptListToTree(dtoList);


    }

    private List<DeptDto> deptListToTree(List<DeptDto> dtoList) {
        if (CollectionUtils.isEmpty(dtoList)) {
            return Lists.newArrayList();
        }
        // level -> [dept1, dept2, ...] Map<String, List<Object>>
        Multimap<String, DeptDto> levelDeptMap = ArrayListMultimap.create();
        List<DeptDto> rootList = Lists.newArrayList();
        for (DeptDto dto : dtoList) {
            levelDeptMap.put(dto.getLevel(), dto);
            if (LevelUtil.ROOT.equals(dto.getLevel())) {
                rootList.add(dto);
            }
        }
        // 按照seq从小到大排序
        Collections.sort(rootList, new Comparator<DeptDto>() {
            public int compare(DeptDto o1, DeptDto o2) {
                return o1.getSort() - o2.getSort();
            }
        });
        // 递归生成树
        transformDeptTree(rootList, LevelUtil.ROOT, levelDeptMap);
        return rootList;
    }

    private void transformDeptTree(List<DeptDto> deptLevelList, String level, Multimap<String, DeptDto> levelDeptMap) {
        for (int i = 0; i < deptLevelList.size(); i++) {
            // 遍历该层的每个元素
            DeptDto deptLevelDto = deptLevelList.get(i);
            // 处理当前层级的数据
            String nextLevel = LevelUtil.calculateLevel(level, deptLevelDto.getId());
            // 处理下一层
            List<DeptDto> tempDeptList = (List<DeptDto>) levelDeptMap.get(nextLevel);
            if (CollectionUtils.isNotEmpty(tempDeptList)) {
                // 排序
                Collections.sort(tempDeptList, deptSeqComparator);
                // 设置下一层部门
                deptLevelDto.setDeptDtos(tempDeptList);
                // 进入到下一层处理
                transformDeptTree(tempDeptList, nextLevel, levelDeptMap);
            }
        }
    }
    public Comparator<DeptDto> deptSeqComparator = new Comparator<DeptDto>() {
        public int compare(DeptDto o1, DeptDto o2) {
            return o1.getSort() - o2.getSort();
        }
    };

}
