package com.xiaodao.modules.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaodao.modules.system.entity.User;
import com.xiaodao.modules.system.mapper.UserMapper;
import com.xiaodao.modules.system.service.IUserService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by xiaodao
 * date: 2019/8/5
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements IUserService {

    @Override
    public List<User> selectUserPage(IPage page, User user) {
        return baseMapper.selectUserPage(page,user);
    }
}
