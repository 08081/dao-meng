package com.xiaodao.modules.system.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by xiaodao
 * date: 2019/8/6
 */
public class LevelUtil {

    public final static String SEPARATE = ".";
    public final static String ROOT = "0";

    public static String calculateLevel(String parentLevel, String parentId){
        if(StringUtils.isBlank(parentLevel)){
            return ROOT;
        }else{
            return StringUtils.join(parentLevel,SEPARATE,parentId);
        }
    }
}
