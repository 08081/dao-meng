package com.xiaodao.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xiaodao.common.persistence.BaseEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by xiaodao
 * date: 2019/8/6
 */
@Getter
@Setter
@TableName("sys_dept")
public class Dept extends BaseEntity<Dept> {
     /* `id` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '部门id',
            `name` varchar(20) COLLATE utf8mb4_bin NOT NULL COMMENT '部门名称',
            `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '商户id',
            `parent_id` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '上级的id',
            `sort` int(11) DEFAULT '0' COMMENT '排序',
            `level` varchar(200) COLLATE utf8mb4_bin DEFAULT '' COMMENT '部门层级',
            `remark` varchar(64) COLLATE utf8mb4_bin DEFAULT '' COMMENT '备注',

            `create_user` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '创建人',
            `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
            `update_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新人',
            `update_time`*/

     private String name;
     private String tenantCode;
     private String parentId;
     private Integer sort;
     private String level;

}
