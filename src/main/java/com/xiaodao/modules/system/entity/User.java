package com.xiaodao.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xiaodao.common.persistence.BaseEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by xiaodao
 * date: 2019/8/2
 */
@Getter
@Setter
@TableName("sys_user")
public class User extends BaseEntity<User> {
    private static final long serialVersionUID = 1L;

    /**
     * 账号
     */
    private String account;
    /**
     * 密码
     */
    private String password;
    /**
     * 昵称
     */
    private String name;
    /**
     * 真名
     */
    private String realName;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 手机
     */
    private String phone;
    /**
     * 生日
     */
    private Date birthday;
    /**
     * 性别
     */
    private Integer sex;
    /**
     * 角色id
     */
    private String roleId;
    /**
     * 部门id
     */
    private String deptId;

    private String tenantId;
}
